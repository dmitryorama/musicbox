﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicBox
{
    public static class AppHelper
    {
        public static void NavigateToErrorPage()
        {
            HttpContext.Current.Server.Transfer("Error.aspx", true);
        }
    }
}