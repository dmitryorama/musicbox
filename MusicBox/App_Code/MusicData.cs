﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MusicBox
{
    public class MusicData
    {
        private string singerName;
        private string songTitle;

        public MusicData(string singer, string songtitle)
        {
            this.singerName = singer;
            this.songTitle = songtitle;
        }

        public string SingerName
        {
            get
            {
                return singerName;
            }
        }

        public string SongTitle
        {
            get
            {
                return songTitle;
            }
        }
    }
}