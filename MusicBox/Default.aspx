﻿<%@ Page Title="<%$ Resources:Strings, SiteTitle %>" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MusicBox.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function CheckSearchClientSide(source, arguments) {
            if (arguments.Value.length >= 3) {
                arguments.IsValid = true;
            }
            else {
                arguments.IsValid = false;
            }
        }
     </script>   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <p>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="buttonSubmit">
        <asp:TextBox ID="textSearch" runat="server"></asp:TextBox>
        <asp:CustomValidator ID="textSearchValidator" runat="server" 
						ControlToValidate="textSearch" 
						ErrorMessage="<%$ Resources:Strings, SearchValidationError %>"
                        ClientValidationFunction="CheckSearchClientSide"
                        ValidationGroup="SearchGroup"
						OnServerValidate="textSearchValidator_ServerValidate" 
						SetFocusOnError="True"
                        Text="*" Display="Dynamic" ValidateEmptyText="True">
        </asp:CustomValidator>
        <asp:Button ID="buttonSubmit" runat="server" 
            ValidationGroup="SearchGroup" 
            Text="<%$ Resources:Strings, SearchButton %>" 
            OnClick="SearchButton_Click" />
        </asp:Panel>
    </p>
    <asp:Repeater id="searchResultsRepeater" runat="server" Visible="False">
        <HeaderTemplate>
            <table>
                <caption><%= GetGlobalResourceObject("Strings", "SearchTableCaption") %></caption>
                <thead>
                    <tr>
                        <th class="singer"><%= GetGlobalResourceObject("Strings", "Singer") %></th>
                        <th class="song"><%= GetGlobalResourceObject("Strings", "Song") %></th>
                        <th class="album"><%= GetGlobalResourceObject("Strings", "Album") %></th>
                        <th class="year"><%= GetGlobalResourceObject("Strings", "Year") %></th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                    <tr>
                        <td> <%# DataBinder.Eval(Container.DataItem, "Singer") %> </td>
                        <td> <%# DataBinder.Eval(Container.DataItem, "Song") %> </td>
                        <td> <%# DataBinder.Eval(Container.DataItem, "Album") %> </td>
                        <td> <%# DataBinder.Eval(Container.DataItem, "Year") %> </td>
                    </tr>
        </ItemTemplate>
        <FooterTemplate>
                </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:Label ID="notFoundLabel" runat="server" Text="<%$ Resources:Strings, MusicNotFound %>" Visible="False"></asp:Label>
    <asp:Label ID="errorMessageLabel" runat="server"></asp:Label>
    <p><asp:ValidationSummary id="SearchValidationSummary" runat="server" DisplayMode="BulletList" ValidationGroup="SearchGroup" ShowMessageBox="False"></asp:ValidationSummary></p>
</asp:Content>
