﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MusicBox
{
    public partial class Default : Page
    {
        protected List<MusicData> musicData;

        //private void getDataLinq()
        //{
        //    using (var context = new Data.DataClassesDataContext())
        //    {
        //        var query = context.Authors.Where(a => a.Name.Length > 4);
        //        var authors = query.ToList();

        //        //SqlMethods.Like(song.Name, "%Me%")

        //        var q2 = from song in context.Songs
        //                 where song.Name.Length > 6
        //                 select new
        //                 {
        //                     Caption = song.Name
        //                 };

        //        var l2 = q2.ToList();
        //    }
        //}

        //private void GetDataEF()
        //{
        //    using (var context = new MusicBox.Entity.MusicBoxEntities3())
        //    {
        //        var q1 = context.Authors.Where(a => a.Name.Length > 6);
        //        var l1 = q1.ToList();
        //        //context.Authors.First(a => a.Id == 1).Name = "Zazie";
        //        //context.SaveChanges();

        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                return;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //musicData = new List<MusicData>();
            //musicData.Add(new MusicData("Muse", "Follow Me"));
            //musicData.Add(new MusicData("Muse", "Hysteria"));
            //musicData.Add(new MusicData("Queen", "Innuendo"));
        }

        protected void textSearchValidator_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (args.Value.Length >= 3 && !args.Value.Any(char.IsDigit))
                args.IsValid = true;
            else
                args.IsValid = false;
        }

        protected override void InitializeCulture()
        {
            try
            {
                if (Session["CurrentUI"] != null)
                {
                    String selectedLanguage = (string)Session["CurrentUI"];
                    UICulture = selectedLanguage;
                    Culture = selectedLanguage;

                    Thread.CurrentThread.CurrentCulture =
                        CultureInfo.CreateSpecificCulture(selectedLanguage);
                    Thread.CurrentThread.CurrentUICulture = new
                        CultureInfo(selectedLanguage);
                }
            }
            catch (CultureNotFoundException)
            {
            }
            finally
            {
                base.InitializeCulture();
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid) 
            { 
                string searchText = textSearch.Text.ToLower();
                
                //var list = musicData.Where(item => 
                //    item.SingerName.ToLower().Contains(searchText) 
                //    || item.SongTitle.ToLower().Contains(searchText))
                //    .ToList<MusicData>();
                
                //if (list.Count > 0)
                //{
                //    searchResultsRepeater.DataSource = list;
                //    searchResultsRepeater.DataBind();
                //    searchResultsRepeater.Visible = true;
                //    notFoundLabel.Visible = false;
                //}
                //else
                //{
                //    searchResultsRepeater.Visible = false;
                //    notFoundLabel.Visible = true;
                //}
                
                using (var context = new MusicBox.Entity.MusicBoxEntities3())
                {
                    //var q1 = context.Authors.Where(a => a.Name.Contains(searchText));
                    //var list = q1.ToList();
                    //context.Authors.First(a => a.Id == 1).Name = "Zazie";
                    //context.SaveChanges();
                    var query = from aas in context.AAS
                                join albums in context.Albums on
                                aas.AlbumID equals albums.Id
                                join songs in context.Songs on
                                aas.SongID equals songs.Id
                                join athsong in context.AuthorSong on
                                songs.Id equals athsong.SongID
                                join authors in context.Authors on
                                athsong.AuthorID equals authors.Id
                                orderby albums.Year, aas.TrackNumber
                                select new
                                {
                                    Singer = authors.Name,
                                    Song = songs.Name,
                                    Album = albums.AlbumName,
                                    Year = albums.Year
                                };

                    var list = query.ToList().Where(
                        item => item.Album.ToLower().Contains(searchText)
                        || item.Singer.ToLower().Contains(searchText)
                        || item.Song.ToLower().Contains(searchText));

                    if (list.Count() > 0)
                    {
                        searchResultsRepeater.DataSource = list;
                        searchResultsRepeater.DataBind();
                        searchResultsRepeater.Visible = true;
                        notFoundLabel.Visible = false;
                    }
                    else
                    {
                        searchResultsRepeater.Visible = false;
                        notFoundLabel.Visible = true;
                    }
                }
            }
        }
    }
}