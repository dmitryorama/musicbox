﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="MusicBox.Error1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
    <h2>ОШИБКА</h2>
    <%  
        var lastError = Server.GetLastError();
        var innerException = lastError != null ? lastError.InnerException : null;
        string errorMessage;
                   
        if (innerException == null)
        {
            errorMessage = lastError == null ? GetGlobalResourceObject("Strings", "ErrorUnknown") as string : lastError.Message;
        }
        else
        {
            errorMessage = innerException.Message;
        }

        Server.ClearError();
    %>
    <%= errorMessage %>
</asp:Content>