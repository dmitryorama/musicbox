﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MusicBox
{
    public partial class Site : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void BtnRU_Click(object sender, EventArgs e)
        {
            //SetCookie("ru");
            SetLanguage("ru");
        }

        protected void BtnEN_Click(object sender, EventArgs e)
        {
            //SetCookie("en");
            SetLanguage("en");
        }

        protected void BtnBY_Click(object sender, EventArgs e)
        {
            SetLanguage("by");
        }

        protected void SetCookie(string lang)
        {
            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = lang;
            Response.SetCookie(cookie);
        }

        protected void SetLanguage(string lang)
        {
            Session["CurrentUI"] = lang;
            Response.Redirect(Request.Url.OriginalString);
        }
       
    }
}